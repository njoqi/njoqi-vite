---
title: Design
name: Design
meta:
  - name: description
    content: Resources and thoughts about design.
---

<p>
  <router-link to="/codex">
    ← Codex index
  </router-link>
</p>

<article-header v-bind="frontmatter" />

> “One could describe Design as a plan for arranging elements to accomplish a particular purpose.”

— Charles Eames

## General

- [Design through the lens of the human condition](http://fadeyev.net/design/)
- [Charles Eames' 30 answers to what is design - 1969](https://flashbak.com/charles-eames-30-answers-to-what-is-design-1969-378352/)
- [Society Centered Design](https://societycentered.design/)
- [5 essential japanese design principles](https://blog.grio.com/2016/03/5-essential-japanese-design-principles.html)

## Information technology

- [The Big Hack](https://bighack.org/)
- [Software disenchantment](https://tonsky.me/blog/disenchantment/)
- [Choose boring technology](https://mcfunley.com/choose-boring-technology)
- [The Demise of the Mildly Dynamic Website](https://www.devever.net/~hl/mildlydynamic)
- [Damaged Earth Catalog](https://damaged.bleu255.com/)
- [Informatics of the oppressed](https://logicmag.io/care/informatics-of-the-oppressed/)
- [Write plain text files](https://sive.rs/plaintext)

## Architecture and infrastructure

- [Domes are very overrated](https://caseyhandmer.wordpress.com/2019/11/28/domes-are-very-over-rated/)
- [A field guide to transmission lines](https://hackaday.com/2019/06/11/a-field-guide-to-transmission-lines/)

## Inspiration sources

- [Spoon &amp; Tamago](https://www.spoon-tamago.com/)
- [Identity Designed](https://identitydesigned.com/)
- [UX Collective](https://uxdesign.cc/)
- [Creative Boom](https://www.creativeboom.com/)

<script setup>
  import ArticleHeader from '../../components/article-header.vue'
</script>