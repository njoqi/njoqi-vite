---
title: Buttons
name: Buttons
meta:
  - name: description
    content: My collection of retro HTML buttons.
---

<p>
  <router-link to="/about">
    ← About
  </router-link>
</p>

<article-header v-bind="frontmatter" />

Websites in the 90s and early 2000s often had these little [88x31 pixels](https://www.quora.com/How-did-the-odd-size-of-88x31-become-a-standard-for-a-website-button) buttons somewhere. Usually to link to other websites, or shout out to/promote something they liked.

Here's some I have made or collected so far.

[![](/assets/images/buttons/button-njoqi.png){height=31 width=88}](https://njoqi.me){.image-link}
![](/assets/images/buttons/button-construction.png){height=31 width=88}
[![](/assets/images/buttons/button-neocities.gif){height=31 width=88}](https://neocities.org/){.image-link}
[![](/assets/images/buttons/button-firefox.gif){height=31 width=88}](https://www.mozilla.org/en-GB/firefox/new/){.image-link}
[![](/assets/images/buttons/button-mastodon.gif){height=31 width=88}](https://joinmastodon.org/){.image-link}
[![](/assets/images/buttons/button-bandcamp.png){height=31 width=88}](https://bandcamp.com/){.image-link}
[![](/assets/images/buttons/button-foobar2000.gif){height=31 width=88}](https://www.foobar2000.org/){.image-link}
[![](/assets/images/buttons/button-minecraft.gif){height=31 width=88}](https://www.minecraft.net){.image-link}
![](/assets/images/buttons/button-trans.gif){height=31 width=88}
![](/assets/images/buttons/button-graphic.png){height=31 width=88}
![](/assets/images/buttons/button-windows.gif){height=31 width=88}
![](/assets/images/buttons/button-digitalme.gif){height=31 width=88}
![](/assets/images/buttons/button-css.png){height=31 width=88}
[![](/assets/images/buttons/button-vscode.gif){height=31 width=88}](https://code.visualstudio.com/){.image-link}
[![](/assets/images/buttons/button-codeberg.png){height=31 width=88}](https://codeberg.org/){.image-link}
![](/assets/images/buttons/button-publicdomain.gif){height=31 width=88}

<script setup>
  import ArticleHeader from '../../components/article-header.vue'
</script>