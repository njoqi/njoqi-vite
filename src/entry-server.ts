import { createApp } from './main';
import { renderToString } from '@vue/server-renderer';
import { renderHeadToString } from '@vueuse/head';
import { RouteLocationRaw } from 'vue-router';

export async function render(url: RouteLocationRaw, manifest: any) {
  const { app, router, head } = createApp();

  // Set the router to the desired URL before rendering
  router.push(url);
  await router.isReady();

  // Passing SSR context object which will be available via useSSRContext()
  // @vitejs/plugin-vue injects code into a component's setup() that registers
  // itself on ctx.modules. After the render, ctx.modules would contain all the
  // components that have been instantiated during this render call.
  const ctx = {};

  type output = {
    appHtml: string,
    headTags: string
  };

  const rendered: output = {
    appHtml: await renderToString(app, ctx),
    headTags: ''
  };
  
  rendered.headTags = renderHeadToString(head).headTags;

  return rendered;
}